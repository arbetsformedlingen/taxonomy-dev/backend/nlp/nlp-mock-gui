(ns nlp-mock-gui.events
  (:require
   [re-frame.core :as re-frame]
   [nlp-mock-gui.db :as db]
   [day8.re-frame.http-fx]
   [ajax.core :as ajax]
   ))

(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))



;; HTTP

(re-frame/reg-event-db
 :process-response
 (fn
   [db [_ response]]           ;; destructure the response from the event vector
   (js/console.log response)
   (print response)
   (let [data (js->clj response)]

     (-> db
         (assoc :data-fetched? :data-fetched) ;; take away that "Loading ..." UI
         (assoc :suggested-concepts data)

         )

     )))



(re-frame/reg-event-db
 :bad-response
 (fn
   [db [_ response]]           ;; destructure the response from the event vector

   (js/console.log response)
   (print response)
   (let [data (js->clj response)]

     (-> db
         (assoc :data-fetched? :error) ;; take away that "Loading ..." UI
         (assoc :error-message (str data))
         )
     )))



#_(fn [e]
  (.preventDefault e)
  (ajax/GET (str "https://taxonomy.api.jobtechdev.se/v1/taxonomy/suggesters/autocomplete?query-string=sotare" )
            {:headers {"api-key" "111"}
             :on-success #(js/console.log (first %))
             :on-error #(js/console.log %)}))



(re-frame/reg-event-fx                              ;; note the trailing -fx
 :fetch-autocomplete                      ;; usage:  (dispatch [:handler-with-http])
 (fn [{:keys [db]} _]                    ;; the first param will be "world"
   {:db   (assoc db :data-fetched? :loading)   ;; causes the twirly-waiting-dialog to show??
    :http-xhrio {:method          :get
                 :headers {"api-key" "111"}
                 :uri             "https://taxonomy.api.jobtechdev.se/v1/taxonomy/suggesters/autocomplete?query-string=clown"
                 :timeout         8000                                           ;; optional see API docs
                 :response-format (ajax.core/json-response-format {:keywords? true})  ;; IMPORTANT!: You must provide this.
                 :on-success      [:process-response]
                 :on-failure      [:bad-response]}}))
