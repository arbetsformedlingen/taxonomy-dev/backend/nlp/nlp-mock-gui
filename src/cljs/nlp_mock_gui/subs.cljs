(ns nlp-mock-gui.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::name
 (fn [db]
   (:name db)))



(re-frame/reg-sub
 ::data-fetched?
 (fn [db]
   (:data-fetched? db)))


(re-frame/reg-sub
 ::suggested-concepts
 (fn [db _]
   (:suggested-concepts db)
   ))
