(ns nlp-mock-gui.views
  (:require
   [re-frame.core :as re-frame]
   [nlp-mock-gui.subs :as subs]
   [ajax.core :as ajax]
   ))



(defn suggested-concept-list-element [data]
  [:li {:key (:taxonomy/id data)} (:taxonomy/preferred-label data)]
  )

(defn suggested-concepts-panel []
  (let [concepts-sub  (re-frame/subscribe [::subs/suggested-concepts])
        concepts @concepts-sub
        _ (js/console.log concepts)
        _ (println concepts)
        ]
    [:ul
     (map suggested-concept-list-element concepts)
     ]
    )
  )



(defn main-panel []
  (let [name (re-frame/subscribe [::subs/name])]
    [:div
     [:h1 "Welcome! Fill out your CV: " @name]

     [:input

      ]

     [:button


      {:on-click
       #(re-frame.core/dispatch [:fetch-autocomplete])
       }

      "Skicka"]

     (suggested-concepts-panel)

     ]))
